/*
* linux-3.0.8/drivers/input/keyboard/gt2440_keydev.c
*/

#include <linux/module.h>
#include <linux/ioport.h>
#include <linux/io.h>
#include <linux/platform_device.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/input.h>

#include <asm/mach-types.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <mach/hardware.h>
#include <mach/regs-gpio.h>


#include "gt2440_key.h"




/***** platf data structure  *****/
static struct gt2440_key_button gt2440_key_buttons[] = {
	{
		.code	= KEY_1,			/* K1 */
		.gpio		= S3C2410_GPF(0),	
		.irq		= IRQ_EINT0,
		.desc		= "K1",
		.active_low	= 1,
		.debounce_interval = 100,
	},
	{
		.code	= KEY_2,			/* K2 */
		.gpio		= S3C2410_GPF(2),	
		.irq		= IRQ_EINT2,
		.desc		= "K2",
		.active_low	= 1,
		.debounce_interval = 100,
	},
	/* used by spi nss pin 
	{
		.code		= KEY_3,
		.gpio		= S3C2410_GPG(3),
		.irq		= IRQ_EINT11,
		.desc		= "K3",
		.active_low	= 1,
		.debounce_interval = 100,
	},
*/
	{
		.code	= KEY_4,
		.gpio		= S3C2410_GPG(11),
		.irq		= IRQ_EINT19,
		.desc		= "K4",
		.active_low	= 1,
		.debounce_interval = 100,
	},
};

static struct gt2440_key_platdata gt2440_key_pdata = {
	.buttons	= gt2440_key_buttons,
	.nbuttons	= ARRAY_SIZE(gt2440_key_buttons),
	.rep = 1,
};




/***** platform device *****/
static void gt2440_platform_device_release(struct device *dev){}
static struct platform_device gt2440_key_device = {
	.name = PLAT_DEVICE_NAME,
	.id = -1,
	//.num_resources	  = ARRAY_SIZE(gt2440_backlight_resource),
	//.resource	  = gt2440_backlight_resource,
	.dev		= {
		.release = gt2440_platform_device_release,
		.platform_data = &gt2440_key_pdata,
	},
};


static int __init gt2440_keydev_init(void)
{	dprintk("gt2440_keydev_init()\n");

	return platform_device_register(&gt2440_key_device);
}
static void __exit gt2440_keydev_exit(void)
{	dprintk("gt2440_keydev_exit()\n");

	platform_device_unregister(&gt2440_key_device);
}

module_init(gt2440_keydev_init);
module_exit(gt2440_keydev_exit);
MODULE_DESCRIPTION("GT2440	Key board Device Register Driver");
MODULE_AUTHOR("Liguang13579<1659890447@qq.com>");
MODULE_LICENSE("GPL v2");




