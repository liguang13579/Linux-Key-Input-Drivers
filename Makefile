KERN_DIR = /opt/2440/linux-3.0.8

all:
	make -C $(KERN_DIR) M=`pwd` modules 

clean:
	make -C $(KERN_DIR) M=`pwd` modules clean
	rm -rf modules.order

obj-m += gt2440_keydev.o
obj-m += gt2440_key.o
