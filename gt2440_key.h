/* 
* linux-3.0.8/drivers/input/keyboard/gt2440_key.h
*/

#ifndef GT2440_KEY_H
#define GT2440_KEY_H




/***** plat data structure  *****/
struct gt2440_key_button {
	unsigned int code;
	int gpio;
	int irq;
	int active_low;
	const char *desc;
	unsigned int type;
	int wakeup;
	int debounce_interval;
	bool can_disable;
	int value;	
};

struct gt2440_key_platdata {
	struct gt2440_key_button *buttons;
	int nbuttons;
	unsigned int poll_interval;	
	unsigned int rep:1;		
	int (*enable)(struct device *dev);
	void (*disable)(struct device *dev);
	const char *name;	
};



/***** driver data structure  *****/
struct gt2440_key_data {
	struct gt2440_key_button *button;
	struct input_dev *input;
	struct timer_list timer;
	struct work_struct work;
	int gpio;
	int irq;
	int timer_debounce;	/* in msecs */
	bool disabled;
};

struct gt2440_key_info {
	struct input_dev *input;
	struct device *dev;
	
	struct mutex disable_lock;
	unsigned int nbuttons;
	int (*enable)(struct device *dev);
	void (*disable)(struct device *dev);
	struct gt2440_key_data data[0];
};


/***** plat device name *****/
#define PLAT_DEVICE_NAME	"gt2440-key"




/***** function declare *****/
static void gt2440_key_report_event(struct gt2440_key_data *data);
static int gt2440_key_open(struct input_dev *input);
static void gt2440_key_close(struct input_dev *input);
static void gt2440_key_timer(unsigned long _data);
static void gt2440_key_work(struct work_struct *work);














/***** debug  *****/
#define GT2440_KEY_DEBUG
#ifdef GT2440_KEY_DEBUG
#define dprintk printk
#else
#define dprintk(...)
#endif


#endif
	
